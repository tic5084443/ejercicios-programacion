#include <stdio.h>

int main(int argc, char *argv[]) {
	
	float lado1, lado2, lado3;
	
	printf("Programa que calcula el perimetro de un triangulo\n\n");
	printf("Al ingresar la longitud de los lados el programa te dira el \nperimetro y el tipo de triangulo que es.");
	
	printf("\n\nIngrese la longitud del primer lado: ");
	scanf("%f", &lado1);
	
	printf("Ingrese la longitud del segundo lado: ");
	scanf("%f", &lado2);
	
	printf("Ingrese la longitud del tercer lado: ");
	scanf("%f", &lado3);
	
	if (lado1 == lado2 && lado2 == lado3) {
		printf("Es un tri�ngulo equil�tero.\n");
		printf("El per�metro es: %.2f\n", 3 * lado1);
	} else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
		printf("Es un tri�ngulo is�sceles.\n");
		printf("El per�metro es: %.2f\n", lado1 + lado2 + lado3);
	} else {
		printf("Es un tri�ngulo escaleno.\n");
		printf("El per�metro es: %.2f\n", lado1 + lado2 + lado3);
	}
	
}
