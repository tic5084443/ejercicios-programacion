#include <stdio.h>

int main() {
	int tabla = 0;
	int auxiliar = 0;
	
	printf("\nPrograma que te dice la tabla que tu quieras\n Siempre y cuando sea del 2 al 10\n");
	
	printf("Escribe la tabla que deseas conocer: ");
	scanf("%i", &tabla);
	
	while (tabla < 2 || tabla > 10) {
		printf("esa tabla est� fuera de rango: ");
		scanf("%i", &tabla);
	}
	
	for (int i = 1; i <= 10; i++) {
		auxiliar = tabla * i;
		printf("%i ", auxiliar);
	}
	
	return 0;
}
