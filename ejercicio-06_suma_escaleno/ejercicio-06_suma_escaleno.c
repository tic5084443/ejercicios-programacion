#include <stdio.h>

int main(int argc, char *argv[]) {
	
	float lado1, lado2, lado3;
	
	printf("Programa que calcula el perimetro de un triangulo escaleno.\n\n");
	
	printf("\n\nIngrese la longitud del primer lado: ");
	scanf("%f", &lado1);
	
	printf("Ingrese la longitud del segundo lado: ");
	scanf("%f", &lado2);
	
	printf("Ingrese la longitud del tercer lado: ");
	scanf("%f", &lado3);
	
	if (lado1 != lado2 && lado2 != lado3) {
		printf("\nEn efecto, es un triángulo escaleno.\n");
		printf("El perímetro es: %.2f\n", lado1 + lado2 + lado3);
	} else {
		printf("\nEs otro tipo de triangulo.\n");
		printf("El perímetro es: %.2f\n", lado1 + lado2 + lado3);
	}
	
	return 0;
}
