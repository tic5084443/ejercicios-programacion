#include <stdio.h>

int esBisiesto(int ano) {
	if ((ano % 4 == 0 && ano % 100 != 0) || (ano % 400 == 0)) {
		return 1;
	} else {
		return 0;
	}
}

int esFechaValida(int dia, int mes, int ano) {
	if (mes < 1 || mes > 12) {
		return 0;
	}
	
	if (dia < 1 || (mes == 2 && dia > 29) || ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia > 30) || (dia > 31)) {
		return 0;
	}
	
	if (mes == 2 && dia == 29 && !esBisiesto(ano)) {
		return 0;
	}
	
	return 1;
}

int main() {
	int dia, mes, ano;
	
	printf("Ingrese el dia: ");
	scanf("%d", &dia);
	
	printf("Ingrese el mes: ");
	scanf("%d", &mes);
	
	printf("Ingrese el ano: ");
	scanf("%d", &ano);
	
	if (esFechaValida(dia, mes, ano)) {
		printf("La fecha es valida.\n");
	} else {
		printf("La fecha es invalida.\n");
	}
	
	return 0;
}

