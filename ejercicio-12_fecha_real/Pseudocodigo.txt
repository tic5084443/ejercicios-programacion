Inicio del programa
    Función esBisiesto(ano)
        Si (ano % 4 == 0 y ano % 100 != 0) o (ano % 400 == 0) entonces
            Devolver 1
        Sino
            Devolver 0
        Fin Si
    Fin Función

    Función esFechaValida(dia, mes, ano)
        Si mes < 1 o mes > 12 entonces
            Devolver 0
        Fin Si

        Si dia < 1 o (mes == 2 y dia > 29) o ((mes == 4 o mes == 6 o mes == 9 o mes == 11) y dia > 30) o (dia > 31) entonces
            Devolver 0
        Fin Si

        Si mes == 2 y dia == 29 y no esBisiesto(ano) entonces
            Devolver 0
        Fin Si

        Devolver 1
    Fin Función

    Procedimiento principal
        Definir dia, mes, ano como enteros

        Imprimir "Ingrese el dia: "
        Leer dia

        Imprimir "Ingrese el mes: "
        Leer mes

        Imprimir "Ingrese el ano: "
        Leer ano

        Si esFechaValida(dia, mes, ano) entonces
            Imprimir "La fecha es valida."
        Sino
            Imprimir "La fecha es invalida."
        Fin Si
    Fin Procedimiento
Fin del programa
