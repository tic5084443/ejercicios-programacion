Definir año como entero

Imprimir "Programa que te dice si un año es bisiesto:"
Leer año desde el usuario

Si (año divisible por 4 y no divisible por 100) o (año divisible por 400):
    Imprimir "El año que ingresaste sí es bisiesto."
Sino:
    Imprimir "El año que ingresaste NO es bisiesto."

Fin del programa
