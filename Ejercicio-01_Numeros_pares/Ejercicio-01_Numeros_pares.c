#include <stdio.h>

int main(int argc, char *argv[]) {
	
	printf("Programa que imprime los numeros pares que hay de 0 a 100:\n");
	
	for(int i = 0; i<101; i++){
		if (i %2 == 0){
			printf("\n%i", i);
		}
	}
}

