#include <stdio.h>

int main(int argc, char *argv[]) {
	
	printf("Programa Ping pong");
	
	for (int i = 1; i <= 100; i++) {
		if (i % 3 == 0 && i % 5 == 0) {
			printf("ping-pong\n\n");
		} else if (i % 3 == 0) {
			printf("ping\n\n");
		} else if (i % 5 == 0) {
			printf("pong\n\n");
		} else {
			printf("%d\n\n", i);
		}
	}
	
}
