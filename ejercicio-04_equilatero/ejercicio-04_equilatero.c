#include <stdio.h>

int main(int argc, char *argv[]) {
	
	float lado1, lado2, lado3;
	
	printf("Programa que calcula el perimetro de un triangulo equilatero\n\n");
	
	printf("\n\nIngrese la longitud del primer lado: ");
	scanf("%f", &lado1);
	
	printf("Ingrese la longitud del segundo lado: ");
	scanf("%f", &lado2);
	
	printf("Ingrese la longitud del tercer lado: ");
	scanf("%f", &lado3);
	
	if (lado1 == lado2 && lado2 == lado3) {
		printf("\nEn efecto, es un tri�ngulo equil�tero.\n");
		printf("El per�metro es: %.2f\n", 3 * lado1);
	} else {
		printf("\nEs un tri�ngulo otro tipo de triangulo.\n");
		printf("El per�metro es: %.2f\n", lado1 + lado2 + lado3);
	}
	
}
