Definir lado1, lado2, lado3 como 0.0

Imprimir "Programa que calcula el perímetro de un triángulo equilátero"

Imprimir "Ingrese la longitud del primer lado:"
Leer lado1 desde el usuario

Imprimir "Ingrese la longitud del segundo lado:"
Leer lado2 desde el usuario

Imprimir "Ingrese la longitud del tercer lado:"
Leer lado3 desde el usuario

Si lado1 es igual a lado2 y lado2 es igual a lado3:
    Imprimir "En efecto, es un triángulo equilátero."
    Imprimir "El perímetro es:", 3 * lado1
Si no:
    Imprimir "Es otro tipo de triángulo."
    Imprimir "El perímetro es:", lado1 + lado2 + lado3

Fin del programa
