#include <stdio.h>

int main(int argc, char *argv[]) {
	
	int mes = 0;
	char *meses[12] = {
			"Enero",
			"Febrero",
			"Marzo",
			"Abril",
			"Mayo",
			"Junio",
			"Julio",
			"Agosto",
			"Septiembre",
			"Octubre",
			"Noviembre",
			"Diciembre"
	};
	
	printf("Digita el mes que quieres conocer: ");
		scanf("%i", &mes);

	while (mes < 1 || mes > 11){
		printf("\nDigita un mes que sea correcto: ");
		scanf("%i", &mes);
	}
		printf("%s", meses[mes]);
}

