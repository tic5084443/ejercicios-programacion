Definir lado1, lado2, lado3 como 0.0

Imprimir "Programa que calcula el perímetro de un triángulo isósceles"

Imprimir "Ingrese la longitud del primer lado:"
Leer lado1 desde el usuario

Imprimir "Ingrese la longitud del segundo lado:"
Leer lado2 desde el usuario

Imprimir "Ingrese la longitud del tercer lado:"
Leer lado3 desde el usuario

Si lado1 es igual a lado2 o lado1 es igual a lado3 o lado2 es igual a lado3:
    Imprimir "En efecto, es un triángulo isósceles."
    Imprimir "El perímetro es:", lado1 + lado2 + lado3
Sino:
    Imprimir "Es otro tipo de triángulo."
    Imprimir "El perímetro es:", lado1 + lado2 + lado3

Fin del programa
